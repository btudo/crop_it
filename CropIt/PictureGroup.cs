﻿//using Microsoft.Win32;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;
//We use forms for OpenFileDialog because Thats the only import of FolderBowserDialog()
using System.Windows.Forms;
using System.Windows.Media;

namespace CropIt
{
   public static class PictureHandler
   {

        static BitmapGroup activePictureGroup;
        static string directory;

        public static string LastCropName;

        public static List<BitmapGroup> LoadDirectory()
        {
            //get unique file names
            List<string> Files = new List<string>();

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    System.Windows.Forms.MessageBox.Show("Files found: " + files.Length.ToString(), "Message");
                }
            }


            return new List<BitmapGroup>();
        }

        public static BitmapGroup LoadFile()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == DialogResult.OK)
            {

            }
                return new BitmapGroup();
        }
           

        public static BitmapGroup LoadPictures()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == DialogResult.OK)
            {
                directory = Path.GetDirectoryName(op.FileName);

                //Remove 0000 from file name;
                string FileWithoutNumbering = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(op.FileName));

                BitmapGroup Group = new BitmapGroup(op.FileName);

                bool formatCheck = false;

                for(int idx = 0; idx<5; idx++)
                {
                    string file = directory + @"\" + FileWithoutNumbering + ".000" + idx + ".png";
                    Console.WriteLine("FileName:" + file + " e:" + File.Exists(file));

                    if(File.Exists(file))
                    {
                        if (idx > 1)
                            formatCheck = true;

                        Bitmap b = new Bitmap(file);  

                        Group.Bitmaps.Add(b); 
                    }                    
                }
                if(!formatCheck)
                {
                    throw new Exception("File Named Incorrectly");
                }

                return Group;
            }
            else
            {
                throw new Exception("File Already Open - Import Failed");
            }            
        }



        public static Bitmap LoadCanvasImage(BitmapGroup bitmapGroup)
        {
            activePictureGroup = bitmapGroup;
            return activePictureGroup.Bitmaps.First();
        }

        public static BitmapGroup CropImage(Int32Rect rect)
        {
            BitmapGroup crops = new BitmapGroup();

            crops.DisplayName = activePictureGroup.DisplayName;

            foreach(Bitmap bitmap in activePictureGroup.Bitmaps)
            {
                Image croppedImage = new Image();
                croppedImage.Width = rect.X;
                croppedImage.Height = rect.Y;
                croppedImage.Margin = new Thickness(5);

                CroppedBitmap cb = new CroppedBitmap(bitmap.Source, rect);       //select region rect

                croppedImage.Source = cb;

                crops.Images.Add(croppedImage);

            }

            return crops;
        }

        //public static BitmapImage CropImage(BitmapImage original)
        //{
        //    double Width = original.Width;
        //    double Height = original.Height;

        //    int newWidth = original.PixelWidth;
        //    int newHeight = original.PixelHeight;
            
        //    Int32Rect rect = new Int32Rect(0, 0, newWidth, newHeight);


        //    CroppedBitmap cb = new CroppedBitmap(original, rect);

        //    PngBitmapEncoder encoder = new PngBitmapEncoder();
        //    MemoryStream memoryStream = new MemoryStream();
        //    BitmapImage bImg = new BitmapImage();


        //    encoder.Frames.Add(BitmapFrame.Create(cb.Source));
        //    encoder.Save(memoryStream);

        //    memoryStream.Position = 0;
        //    bImg.BeginInit();
        //    bImg.StreamSource = memoryStream;
        //    bImg.EndInit();

        //    memoryStream.Close();

        //    return bImg;
        //}


        public static bool SaveCrops(BitmapGroup Crops, string FileName)
        {
            string CropDirectory = Crops.DisplayName + "_Crops";
            string exportDirectory = directory + @"\" + CropDirectory + @"\";

            bool directoryExists = System.IO.Directory.Exists(exportDirectory);

            LastCropName = FileName;

            if (!directoryExists)
            {
                //check if directory is writeable
                Directory.CreateDirectory(exportDirectory);
            }

            int idx = 1;

            foreach (Image image in Crops.Images.Skip(1))
            {
                string FilePath = exportDirectory + FileName + "_000" + idx++.ToString() + ".png";

                    using (FileStream fs = new FileStream(FilePath, FileMode.Create))
                    {
                        PngBitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create((BitmapSource)image.Source));
                        encoder.Save(fs);
                    }
            }

            return true;
        }
        

        public static void AddRect(String name, Int32Rect rect, Canvas canvas)
        {
            System.Windows.Shapes.Rectangle rectangle = new System.Windows.Shapes.Rectangle();

            rectangle.Width = rect.Width;
            rectangle.Height = rect.Height;
            rectangle.Fill = new SolidColorBrush() { Color = Color.FromRgb(93,00,93), Opacity = 0.75f };

            System.Windows.Controls.Label rectLabel = new System.Windows.Controls.Label();

            rectLabel.Content = name;
            rectLabel.Foreground = Brushes.White;


            canvas.Children.Add(rectangle);
            Canvas.SetLeft(rectangle, rect.X);
            Canvas.SetTop(rectangle, rect.Y);

            canvas.Children.Add(rectLabel);
            Canvas.SetLeft(rectLabel, rect.X);
            Canvas.SetTop(rectLabel, rect.Y);

        }
    }

    public class Bitmap
    {
        public BitmapImage Source { get; set; }
        public string DisplayName { get; set; }
        public bool Valid { get; set; } = true;

        public Bitmap(string FileName)
        {
            Source = new BitmapImage(new Uri(FileName));
            DisplayName = Path.GetFileNameWithoutExtension(FileName);

            if (Source.Height != Source.PixelHeight || Source.Width != Source.PixelWidth)
            {
                ConsoleBox.WriteError("Height:" + Source.Height + " Pixel Height:" + Source.PixelHeight);


                Valid = false;           
            }
        
            
        }

        public Bitmap(BitmapImage image)
        {
            Source = image ?? throw new NullReferenceException();
        }

        //public void GetResolution(Stream stream)
        //{
        //    //IHDR 
        //    byte[] pattern = new byte[] { 0x49, 0x48, 0x44, 0x54 };

        //    byte[] photo = ReadFully(stream);

        //    long length = stream.Length;

        //    int ihdrOffset = Search(photo, pattern);

        //    Console.WriteLine("Offset:" + ihdrOffset);

        //}




        //public static byte[] ReadFully(Stream input)
        //{
        //    byte[] buffer = new byte[input.Length*2 + 1];
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        int read;
        //        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        //        {
        //            ms.Write(buffer, 0, read);
        //        }
        //        return ms.ToArray();
        //    }
        //}


        //int Search(byte[] src, byte[] pattern)
        //{

        //    int c = src.Length - pattern.Length + 1;
        //    int j;
        //    for (int i = 0; i < c; i++)
        //    {
        //        if (src[i] != pattern[0]) continue;
        //        for (j = pattern.Length - 1; j >= 1 && src[i + j] == pattern[j]; j--) ;
        //        if (j == 0) return i;
        //    }
        //    return -1;
        //}

    }

    public class BitmapGroup
    {    
        public List<Bitmap> Bitmaps = new List<Bitmap>();
        public string DisplayName { get; set; }
        public List<Image> Images = new List<Image>();

        public BitmapGroup(string FileName)
        {
            //needs to be done more generically - check for peroid, if peroid exists. GetWithout()
            
            DisplayName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(FileName)));
        }

        public BitmapGroup()
        {

        }

        public bool Valid
        {
            get
            {
                Bitmap b = Bitmaps.Find(x => x.Valid == false);

                if (b == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }

    public class BitmapGroupNode : TreeViewItem
    {
        public BitmapGroupNode(BitmapGroup bitmapGroup)
        {
            Header = bitmapGroup.DisplayName;
            BitmapGroup = bitmapGroup;
            BitmapGroup.Bitmaps.ForEach(x => Items.Add(new BitmapNode(x)));
        }

        public BitmapGroup BitmapGroup
        {
            get;
            private set;
        }

    }

    public class BitmapNode : TreeViewItem
    {
        public BitmapNode(Bitmap bitmap)
        {
            Header = bitmap.DisplayName;
            Bitmap = bitmap;
        }

        public Bitmap Bitmap
        {
            get;
            private set;
        }
    }
}
