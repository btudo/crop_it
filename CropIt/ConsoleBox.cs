﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace CropIt
{
    public static class ConsoleBox
    {
        public static TextBox box;

        public static void WriteLine(string s)
        {
            box.Foreground = Brushes.Black;
            box.AppendText(s + "\n");
            box.ScrollToEnd();
        }

        public static void WriteError(string s)
        {
            box.Foreground = Brushes.Red;

            box.AppendText(s + "\n");
            box.ScrollToEnd();
        }

    }
}
