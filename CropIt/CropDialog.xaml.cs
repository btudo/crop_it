﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CropIt
{
    /// <summary>
    /// Interaction logic for CropDialog.xaml
    /// </summary>
    public partial class CropDialog : Window
    {
        private BitmapGroup CropGroup = new BitmapGroup();

        public string CropName { get; set; }
        public bool Accepted { get; set; } = false;
        //public 


        //@Depreciated
        public CropDialog(Image image1,Image image2, Image image3, Image image4)
        {
           
            InitializeComponent();

            Image1.Source = image1.Source;
            Image2.Source = image1.Source;
            Image3.Source = image1.Source;
            Image4.Source = image1.Source;

        }

        public CropDialog(BitmapGroup bitmapGroup, string LastGroupName)
        {
            CropGroup.DisplayName = bitmapGroup.DisplayName;
            CropGroup.Images = bitmapGroup.Images;

            //CropGroup = bitmapGroup;

            InitializeComponent();

            GroupTextBox.Text = LastGroupName;
            GroupTextBox.CaretIndex = LastGroupName?.Length + 2 ?? 0;

            int len = bitmapGroup.Images.Count;

            Console.WriteLine("Length:" + len);

            if (len > 1)
                Image1.Source = bitmapGroup.Images[1].Source;

            if (len > 2)
                Image2.Source = bitmapGroup.Images[2].Source;

            if (len > 3)
                Image3.Source = bitmapGroup.Images[3].Source;

            if (len >= 4)
                Image4.Source = bitmapGroup.Images[4].Source;


        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            PictureHandler.SaveCrops(CropGroup, GroupTextBox.Text);
            CropName = GroupTextBox.Text;
            Accepted = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            //Close();
        }
    }
}
