﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CropIt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        

        public MainWindow()
        {
            InitializeComponent();
            ConsoleBox.box = ConsoleTextBox;
        }




        private void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
            }

        }


        private void MenuOpen_Click(object sender, RoutedEventArgs e)
        {
            List<BitmapGroupNode> nodes = new List<BitmapGroupNode>();


            try
            {
                BitmapGroup b = new BitmapGroup();
                b = PictureHandler.LoadPictures();



                if (!b.Valid)
                {
                    throw new Exception("File Dimensions Not Valid");

                }
                else
                {
                    nodes.Add(new BitmapGroupNode(b));
                    PictureHandler.LoadCanvasImage(nodes.First().BitmapGroup);
                    imgPhoto.Source = PictureHandler.LoadCanvasImage(nodes.First().BitmapGroup).Source;
                }


                
            }
            catch (Exception ex)
            {

                ConsoleBox.WriteError("Exception " + ex.Message);
                ConsoleBox.WriteLine("ReSave Image title.0000.png in mspaint.exe");
            }

            
            //nodes.Add(new BitmapGroupNode(PictureHandler.LoadPictures()));

            FileTreeView.ItemsSource = nodes;

        }


        private void OnItemMouseDoubleClick(object sender, MouseButtonEventArgs args)
        {
            if (sender is TreeViewItem)
            {
                if (!((TreeViewItem)sender).IsSelected)
                {
                    return;
                }

                if(sender is BitmapGroupNode)
                {
                    LoadFiles((sender as BitmapGroupNode).BitmapGroup);

                }
            }
        }

        private void LoadFiles (BitmapGroup group)
        {
            imgPhoto.Source = PictureHandler.LoadCanvasImage(group).Source;

            ConsoleBox.WriteLine("Group:" + group.DisplayName);
        }


        bool mouseDown = false; // Set to 'true' when mouse is held down.
        Point mouseDownPos; // The point where the mouse button was clicked down.

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // Capture and track the mouse.
            mouseDown = true;
            mouseDownPos = e.GetPosition(theGrid);
            theGrid.CaptureMouse();

            // Initial placement of the drag selection box.         
            Canvas.SetLeft(selectionBox, mouseDownPos.X);
            Canvas.SetTop(selectionBox, mouseDownPos.Y);
            selectionBox.Width = 0;
            selectionBox.Height = 0;

            // Make the drag selection box visible.
            selectionBox.Visibility = Visibility.Visible;
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // Release the mouse capture and stop tracking it.
            mouseDown = false;
            theGrid.ReleaseMouseCapture();

            // Hide the drag selection box.
            selectionBox.Visibility = Visibility.Collapsed;

            Point mouseUpPos = e.GetPosition(theGrid);

            // TODO: 
            //
            // The mouse has been released, check to see if any of the items 
            // in the other canvas are contained within mouseDownPos and 
            // mouseUpPos, for any that are, select them!
            //



            double rectX = Math.Round(mouseDownPos.X);
            double rectY = Math.Round(mouseDownPos.Y);

            double rectWidth = Math.Round(mouseUpPos.X - mouseDownPos.X);
            double rectHeight = Math.Round(mouseUpPos.Y - mouseDownPos.Y);

            if(rectX > 0 && rectY > 0  && Math.Abs(rectWidth) > 3 && Math.Abs(rectHeight) > 3)
            {
                if(rectWidth < 0)
                {
                    rectX += rectWidth;
                    rectWidth = Math.Abs(rectWidth);
                    
                }

                if (rectHeight < 0)
                {
                    rectY += rectHeight;
                    rectHeight = Math.Abs(rectHeight);

                }

                Int32Rect CropCoordinates = new Int32Rect((int)rectX, (int)rectY, (int)rectWidth, (int)rectHeight);

                ConsoleBox.WriteLine("Coords:" + CropCoordinates.ToString());

                BitmapGroup bitGroup  = PictureHandler.CropImage(CropCoordinates);

                //Image croppedImage = new Image();
                //croppedImage.Width = rectWidth;
                //croppedImage.Height = rectHeight;
                //croppedImage.Margin = new Thickness(5);

                //CroppedBitmap cb = new CroppedBitmap(imgPhoto.Source as BitmapImage, new Int32Rect((int)rectX, (int)rectY,(int)rectWidth , (int)rectHeight));       //select region rect
                //croppedImage.Source = cb;

                //new CropDialog(croppedImage, croppedImage, croppedImage, croppedImage).ShowDialog();

                CropDialog dialog = new CropDialog(bitGroup,PictureHandler.LastCropName);

                dialog.ShowDialog();

                if(dialog.Accepted)
                {
                    PictureHandler.AddRect(dialog.CropName, CropCoordinates,MainCanvas);
                    ConsoleBox.WriteLine(dialog.CropName+" (" + rectX + "," + rectY + "): Width:" + rectWidth + " Height:" + rectHeight);
                }



            }

      
            //mouseDownPos.X
      

            //ConsoleBox.AppendText("(" + rectX + "," +rectY+ "): Width:"+ rectWidth + " Height:" +rectHeight+"\n");
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                // When the mouse is held down, reposition the drag selection box.

                Point mousePos = e.GetPosition(theGrid);

                if (mouseDownPos.X < mousePos.X)
                {
                    Canvas.SetLeft(selectionBox, mouseDownPos.X);
                    selectionBox.Width = mousePos.X - mouseDownPos.X;
                }
                else
                {
                    Canvas.SetLeft(selectionBox, mousePos.X);
                    selectionBox.Width = mouseDownPos.X - mousePos.X;
                }

                if (mouseDownPos.Y < mousePos.Y)
                {
                    Canvas.SetTop(selectionBox, mouseDownPos.Y);
                    selectionBox.Height = mousePos.Y - mouseDownPos.Y;
                }
                else
                {
                    Canvas.SetTop(selectionBox, mousePos.Y);
                    selectionBox.Height = mouseDownPos.Y - mousePos.Y;
                }
            }
        }

  

        private void MenuFileDirectory_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

